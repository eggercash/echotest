<?php

use Faker\Generator as Faker;

$factory->define(\App\Game::class, function (Faker $faker) {
    return [
        'title' => $faker->word
    ];
});
