<?php

namespace App\Events;

use App\Game;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GameOpen implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $game;
    public $lastBet;

    public function __construct(Game $game, $lastBet)
    {
        $this->game = $game;
        $this->lastBet = $lastBet;
    }

    public function broadcastOn()
    {
        return new PrivateChannel('game-'.$this->game->getKey());
    }

    public function broadcastAs()
    {
        return 'bet';
    }
}
