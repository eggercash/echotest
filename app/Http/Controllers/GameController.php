<?php

namespace App\Http\Controllers;

use App\Bets;
use App\Events\GameOpen;
use App\Events\TimerStart;
use App\Game;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class GameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $games = Game::all();

        return view('games', compact('games'));
    }

    public function open($id)
    {
        $game = Game::findOrFail($id);

        return view('open', compact('game'));
    }

    public function bet(Request $request)
    {
        $game = Game::findOrFail($request->game_id);

        $game->bets()->create([
            'user_id' => Auth::id(),
            'amount' => 100,
        ]);


        //Проверка на ставку разных пользователей
        $bets = Bets::hasDifferenceBets($game->getKey())->first();

        if($bets){
            if(is_null($game->started_at)) {
                $game->started_at = microtime(true) * 1000;
                $game->save();
            }

            broadcast(new TimerStart($game));
        }



//        // Example publish by Redis, without Event
//        Redis::publish('private-game-'.$game->getKey(), json_encode([
//            'event' => 'bet',
//            'data' => [
//                'game' => $game,
//                'lastBet' => 100
//            ]
//        ]));

        broadcast(new GameOpen($game, 100))->toOthers();

        return response()->json(null, 200);
    }
}
