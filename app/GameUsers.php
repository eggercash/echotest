<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GameUsers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'game_id', 'user_id'
    ];

    public function scopeGetGameById($query, $game_id)
    {
        return $query->where('game_id', $game_id)->where('user_id', Auth::id());
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
