<?php

namespace App\Services\Random;

use GuzzleHttp\Client;

class RandomApi
{
    protected $url = 'https://api.random.org/json-rpc/1/invoke';
    protected $verify = 'https://api.random.org/verify';
    protected $method = 'generateSignedIntegers';
    protected $client = null;

    /**
     * The lower boundary for the range from which the random numbers will be picked.
     * Must be within the [-1e9,1e9] range.
     *
     * @var int
     */
    protected $min = 0;

    /**
     * The upper boundary for the range from which the random numbers will be picked.
     * Must be within the [-1e9,1e9] range.
     *
     * @var int
     */
    protected $max = 10;

    /**
     * Specifies whether the random numbers should be picked with replacement.
     * The default (true) will cause the numbers to be picked with replacement, i.e.,
     * the resulting numbers may contain duplicate values (like a series of dice rolls).
     * If you want the numbers picked to be unique (like raffle tickets drawn from a container),
     * set this value to false.
     *
     * @var bool
     */
    protected $replacement = true;

    /**
     * @var int
     *
     * Specifies the base that will be used to display the numbers.
     * Values allowed are 2, 8, 10 and 16.
     */
    protected $base = 10;

    /**
     * How many random integers you need.
     * Must be within the [1,1e4] range.
     *
     * @var int
     */
    protected $n = 1;

    /**
     * RandomApi constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Set the lower boundary for the range from which the random numbers will be picked.
     *
     * @param $min
     */
    public function setMin($min)
    {
        $this->min = $min;
    }

    /**
     * Set the upper boundary for the range from which the random numbers will be picked
     *
     * @param $max
     */
    public function setMax($max)
    {
        $this->max = $max;
    }

    /**
     * Send request to Random.org api & get RandomResponse instance
     */
    public function generate()
    {
        $response = $this->client->request('POST', $this->url, [
            'json' => [
                'id' => uniqid(),
                'jsonrpc' => '2.0',
                'method' => $this->method,
                'params' => [
                    'apiKey' => env('RANDOM_API_KEY'),
                    'n' => $this->n,
                    'min' => $this->min,
                    'max' => $this->max,
                    'replacement' => $this->replacement,
                    'base' => $this->base
                ]
            ]
        ]);

        $body =  $response->getBody();
        $json = json_decode($body->getContents(), true);

        return new RandomResponse($json);
    }

    /**
     * Verifying the signature of a response
     *
     * @param $data
     * @return string
     */
    public function verify($data)
    {
        $response = $this->client->request('POST', $this->verify, [
            'form_params' => [
                'format' => 'json',
                'random' => json_encode([
                    'method' => 'generateSignedIntegers',
                    'hashedApiKey' => $data['hashedApiKey'],
                    'n' => $data['n'],
                    'min' => $data['min'],
                    'max' => $data['max'],
                    'replacement' => true,
                    'base' => 10,
                    'data' => [
                        $data['data']
                    ],
                    'completionTime' => $data['completionTime'],
                    'serialNumber' => $data['serialNumber'],
                ]),
                'signature' => $data['signature']
            ]
        ]);

        $body =  $response->getBody();

        return $body->getContents();
    }
}