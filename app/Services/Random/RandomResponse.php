<?php

namespace App\Services\Random;

class RandomResponse
{
    /**
     * Response from Random.org api
     *
     * @var null
     */
    protected $response = null;

    /**
     * RandomResponse constructor.
     * @param $response
     */
    public function __construct($response)
    {
        $this->response = $response;
    }

    /**
     * Checking request for success
     *
     * @return bool
     */
    public function isSuccess()
    {
        if(isset($this->response['result']))
            return true;
        return false;
    }

    /**
     * Receiving winner data from the response
     *
     * @return array|null
     */
    public function getWinners()
    {
        if(isset($this->response['result']['random']['data']))
            return $this->response['result']['random']['data'];
        return null;
    }

    /**
     * Receiving error data from the response
     *
     * @return array|null
     */
    public function getErrorData()
    {
        if(isset($this->response['error']))
            return $this->response['error'];
        return null;
    }
}