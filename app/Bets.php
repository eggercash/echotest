<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Bets extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'game_id', 'amount'
    ];

    public function scopeHasDifferenceBets($query, $game_id)
    {
        $query->where('game_id', $game_id)->where('user_id','<>', Auth::id());
    }
}
