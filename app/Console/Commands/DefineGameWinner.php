<?php

namespace App\Console\Commands;

use App\Events\GameWinner;
use App\Game;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DefineGameWinner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'game:winner';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Define game winner';

    /**
     * Indicates whether the command should be shown in the Artisan command list.
     *
     * @var bool
     */
    protected $hidden = true;


    public $stop = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $game = Game::find(1);
        $user = User::find(1);

        broadcast(new GameWinner($user, $game));

        sleep(1);
    }
}
