<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Game extends Model
{

    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'bank'
    ];

    protected $with = ['bets'];

    public function bets()
    {
        return $this->hasMany(Bets::class);
    }
}
