### Запуск ###

* art migrate --seed
* В файлей .env установить следующие значения BROADCAST_DRIVER=redis, QUEUE_DRIVER=redis
* laravel-echo-server init (настройка)
* laravel-echo-server start
* Для настройки админки, в .env файле установить следующиее значение ADMIN_URL='yourAdminDomain'
* Для настройки демона, скопировать файл `define-game-winner.conf` в папку `/etc/supervisor/conf.d/`, изменить значения на свои, выполнить команды `sudo supervisorctl reread`, `sudo supervisorctl update`, `sudo supervisorctl start define-game-winner:*`
* Тестирование Random.org api - /random-api - генератор, /random-api-verify - верификация