<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the 'web' middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('game', 'GameController@index');
Route::get('game/{id}', 'GameController@open')->name('game.open');
Route::post('game/bet', 'GameController@bet')->name('game.bet');

Route::get('random-api', function (){
    $api = new \App\Services\Random\RandomApi();

    $api->setMin(1);
    $api->setMax(100);
    $response =  $api->generate();

    dd($response);
});

Route::get('random-api-verify', function (){

    $data = [
        'hashedApiKey' => 'IE7W+BQQgx1iRYdn/H+zmYBCcTaHfr08oMpzlvx3cmfngnkmWw2FdZaJHcO2XVsZXch8KkV2QNIgS7+Rnr/d5w==',
        'n' => 1,
        'min' => 1,
        'max' => 100,
        'data' => 77,
        'completionTime' => '2018-02-20 09:07:10Z',
        'serialNumber' => 42,
        'signature' => 'KxjLU6UDrG4wHf7w2AcUPqGKctKhEKgktgu3UM3mwnf2YAoTM9MgzfAe/r0wIlI2puI+dZcvlDREZ15+5kGsN3EvdVzi+6LP2Q1VgppGriJZPMKj5meqSUeau8HAorPbwLzkzUn5Nbp+EHTF1+xNqIap0RQi+AbXQ3LY5yr+0eciZNYq7CX5gc81YgDhNhB00LLgQXM9B1RHvBD2UA1M1uE5OKhXhJMoa5L8gsxMEQbpMqUMii8mJ9i9L90Zi7jGxOanlUsXpQIYmngI12HeXGmcxukqsOjAx6GWeowuEkykp9HuAvLJ0qGdtZRh7aEcXtF6Z8CwFmq/wg4EDvv3qBh/0YWbEqF0nyjsF3m9rCM9nDOsHe6WjaBQY13wrNB0zNBYAIgA+pB7aOkFstMc0UJRJhTqRWRNQdzHHRYl8uRclopvRdWMeUooicsTn+0rfp9+F5ZOZPZt3U5Z+Tlv3LCryUxMzg9qceD8eZ45A+uFGMuEGnSQgQrlw0dIrTKUA4YO07WHqScgHTV7uALzA3QO4Sq2u0CvwA92KajEf3IsoaLgiOBEO8d+Tlkx/pp+2wnyzp3ct2lRSsvftwd0T1PXnrcSHWRff1tdHaIBarIRQgxvZ01jc970u/lK/sdgle/B2rQZU3Pu9L86dBUZ68DVz6S/1I9v2nzGnOIxYSU='
    ];

    $api = new \App\Services\Random\RandomApi();

    $response = $api->verify($data);

    return view('verify', [
        'html' => $response
    ]);
});
