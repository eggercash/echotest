<?php

/*
|--------------------------------------------------------------------------
| Backpack\PermissionManager Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are
| handled by the Backpack\PermissionManager package.
|
*/


Route::group([
    'domain' => env('ADMIN_URL'),
    'namespace'  => 'App\Http\Controllers\Admin',
    'middleware' => ['web','auth:manager'],
], function () {
    CRUD::resource('permission', 'PermissionCrudController');
    CRUD::resource('role', 'RoleCrudController');
    CRUD::resource('manager', 'UserCrudController');
});