@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($games as $game)
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{$game->title}}</div>
                        <div class="panel-body">
                            <a href="{{route('game.open', $game->getKey())}}" class="btn btn-primary">Open</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
