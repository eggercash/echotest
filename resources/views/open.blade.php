@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$game->title}} <div class="pull-right" id="bank">Bank: <span>{{$game->bets->sum('amount')}}</span></div></div>
                    <div class="panel-body">
                        <ul id="conns_list"></ul>
                        <button id="bet" class="btn btn-primary">Bet</button>
                    </div>
                </div>
                <div>
                    <span id="timer">Не определен</span>
                </div>
                <div id="notifications"></div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var game_id = '{{$game->getKey()}}';
        var user_id = '{{\Illuminate\Support\Facades\Auth::id()}}';
        var timer = null;


        // Уведомления для пользователя
        Echo.private('App.User.' + user_id)
            .notification(function (notification) {
                $('#notifications').append(notification.message);
            });

        // Уведомления для игры
        // Echo.private('App.Game.' + game_id)
        //     .notification(function (notification) {
        //         $('#notifications').append(notification.message);
        //     });

        // Приватный канал игры
        Echo.private('game-' + game_id)
            .notification(function (notification) {
                        $('#notifications').append(notification.message);
                    })
        .listen('.bet', function(e){
            var bank = parseInt($('#bank>span').text()) + e.lastBet;
            $('#bank>span').html(bank);
        })
        .listen('.timerStart', function(e){
            if(!timer){
                timerStart(e.game.started_at);
            }
        })
        .listen('.winner', function(e){
            console.log('Winner', e);
        });

        // Канал присутсвия игры
        Echo.join('game-{{$game->getKey()}}')
        .here(function (users) {

            timerStart('{{$game->started_at}}');

            var list = '';
            $.each(users, function(index, value ) {
                list+='<li id="'+value.id+'">' + value.name + ' --- <span id="'+value.id+'-chance">0</span></li>';
            });
            $('#conns_list').html(list);
        })
        .joining(function(user){
            if($('li#'+user.id).length == 0) {
                $('#conns_list').append('<li id="'+user.id+'">' + user.name + ' --- <span id="'+ user.id+'-chance">0</span></li>');
            }
        })
        .leaving(function (user) {
            if($('li#'+user.id).length == 1) {
                $('li').remove('#'+user.id);
            }
        });

        // Действие ставка
        $('#bet').on('click', function () {
            // Сразу рисуем у того, кто ставит ставку
            var bank = parseInt($('#bank>span').text()) + 100;
            $('#bank>span').html(bank);
            $.post({
                url : 'bet',
                data : {game_id : game_id}
            });
        });

        // Таймер
        {{--var date = moment('{{$game->start_at}}').unix();--}}
        {{--var countdown = date - moment().unix();--}}

        {{--var timer{{$game->getKey()}} = null;--}}

        {{--if(countdown > 0) {--}}
            {{--clearInterval(timer{{$game->getKey()}});--}}
            {{--var timer{{$game->getKey()}} = setInterval(function() {--}}
                {{--countdown--;--}}

                {{--if(countdown === 0){--}}
                    {{--clearInterval(timer{{$game->getKey()}});--}}
                    {{--alert('Finish');--}}
                {{--}--}}
                {{--$('#timer').html(countdown);--}}
            {{--}, 1000);--}}
        {{--}--}}

        function timerStart(started_at)
        {
            // Таймер
            var diff = (new Date().getTime() - started_at) / 1000;
            var countdown = Math.round(30 - diff);
            if(countdown > 0) {
                timer = setInterval(function() {
                    countdown--;

                    if(countdown <= 0){
                        clearInterval(timer);
                        alert('Finish');
                    }
                    $('#timer').html(countdown);
                }, 1000);
            }
        }
    </script>
@endsection
